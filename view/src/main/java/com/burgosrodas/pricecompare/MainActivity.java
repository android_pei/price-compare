package com.burgosrodas.pricecompare;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.burgosrodas.pricecompare.common.base.BaseActivity;

import io.reactivex.Observable;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }
}
